STAGING_HOST=opal5.opalstack.com
STAGING_PORT=22
STAGING_USER=manufactura
STAGING_TARGET_DIR=/home/manufactura/apps/mur-doku/lib/tpl/dokumur/

LIVE_HOST=login.mur.at
LIVE_PORT=22
LIVE_USER=wissensbasis
LIVE_TARGET_DIR=/var/www/limes/wissensbasis.mur.at/webseite/lib/tpl/dokumur/

deploy: 
	rsync -e "ssh -p $(STAGING_PORT)" -P -rvzc dokumur/ $(STAGING_USER)@$(STAGING_HOST):$(STAGING_TARGET_DIR) --cvs-exclude

dry-deploy: 
	rsync -n -e "ssh -p $(STAGING_PORT)" -P -rvzc dokumur/ $(STAGING_USER)@$(STAGING_HOST):$(STAGING_TARGET_DIR) --cvs-exclude

live-deploy: 
	rsync -e "ssh -p $(LIVE_PORT)" -P -rvzc dokumur/ $(LIVE_USER)@$(LIVE_HOST):$(LIVE_TARGET_DIR) --cvs-exclude

live-dry-deploy: 
	rsync -n -e "ssh -p $(LIVE_PORT)" -P -rvzc dokumur/ $(LIVE_USER)@$(LIVE_HOST):$(LIVE_TARGET_DIR) --cvs-exclude

.PHONY: deploy dry-deploy live-deploy live-dry-deploy
