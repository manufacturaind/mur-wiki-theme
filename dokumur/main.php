<?php
/**
 * Murat 2022 - based in dokuwiki default template
 *
 * @link     http://dokuwiki.org/template
 * @author   Anika Henke <anika@selfthinker.org>
 * @author   Clarence Lee <clarencedglee@gmail.com>
 * @license  GPL 2 (http://www.gnu.org/licenses/gpl.html)
 */

if (!defined('DOKU_INC')) die(); /* must be run from within DokuWiki */

$hasSidebar = page_findnearest($conf['sidebar']);
$showSidebar = $hasSidebar && ($ACT=='show');
?><!DOCTYPE html>
<html lang="<?php echo $conf['lang'] ?>" dir="<?php echo $lang['direction'] ?>" class="no-js">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?php tpl_pagetitle() ?> [<?php echo strip_tags($conf['title']) ?>]</title>
    <script>(function(H){H.className=H.className.replace(/\bno-js\b/,'js')})(document.documentElement)</script>
    <?php tpl_metaheaders() ?>
    <?php echo tpl_favicon(array('favicon', 'mobile')) ?>
    <?php tpl_includeFile('meta.html') ?>
</head>

<body>
    <div id="dokuwiki__site"><div id="dokuwiki__top" class="site <?php echo tpl_classes(); ?> <?php
        echo ($showSidebar) ? 'showSidebar' : ''; ?> <?php echo ($hasSidebar) ? 'hasSidebar' : ''; ?>">

        <?php include('tpl_header.php') ?>

        <div class="wrapper">

            <div class="sidebar-overlay"></div>

                <!-- ********** Sidebar ********** -->
                <nav class="sidebar" aria-label="<?php echo $lang['sidebar']
                    ?>">
                    <div class="search action">
                        <?php tpl_searchform(); ?>
                    </div>
                    <div class="content">
                        <?php tpl_flush() ?>
                        <?php tpl_includeFile('sidebarheader.html') ?>
                        <?php tpl_include_page($conf['sidebar'], true, true) ?>
                        <?php tpl_includeFile('sidebarfooter.html') ?>
                    </div>
                    <div class="sitemenu">
                      <h3>Utilities</h3>
      								<ul>
      									<?php
      									$menu_items = (new \dokuwiki\Menu\SiteMenu())->getItems();
      									foreach($menu_items as $item) {
      									echo '<li class="'.$item->getType().'">'
      										.'<a class="" href="'.$item->getLink().'" title="'.$item->getTitle().'">'
      										. $item->getLabel()
      										. '</a></li>';
      									}

      									?>
      								</ul>
                    </div>
                </nav><!-- /sidebar -->

            <!-- ********** CONTENT ********** -->
            <main id="dokuwiki__content">

                <div id="dokuwiki_notifications"><?php html_msgarea() ?></div>

                <!-- SITE TOOLS -->
                <div id="dokuwiki__sitetools">
                    <div class="mobileTools">
                        <?php echo (new \dokuwiki\Menu\MobileMenu())->getDropdown($lang['tools']); ?>
                    </div>
                    <div class="desktopTools">
                      <ul>
                        <?php
      									$menu_items = (new \dokuwiki\Menu\PageMenu())->getItems();
      									foreach($menu_items as $item) {
      									echo '<li class="'.$item->getType().'">'
      										.'<a class="'.$item->getLinkAttributes('')['class'].'" href="'.$item->getLink().'" title="'.$item->getTitle().'">'
      										. $item->getLabel()
      										. '</a></li>';
      									}
      									?>
                      </ul>
                    </div>
                </div><!-- /#dokuwiki__sitetools -->
                <div class="page">
                    <?php tpl_flush() ?>
                    <?php tpl_includeFile('pageheader.html') ?>
                    <!-- wikipage start -->
                    <div id="page-toc"><?php tpl_toc()?></div>
                    <div id="page-content"><?php tpl_content(false) ?>
                      <div class="docInfo"><?php tpl_pageinfo() ?></div>
                      <!-- PAGE ACTIONS -->
                      <nav id="dokuwiki__pagetools" aria-labelledby="dokuwiki__pagetools__heading">
                        <ul class="pagetools">
                          <?php
                          $menu_items = (new \dokuwiki\Menu\PageMenu())->getItems();
                          foreach($menu_items as $item) {
                          echo '<li class="'.$item->getType().'">'
                            .'<a class="button '.$item->getLinkAttributes('')['class'].'" href="'.$item->getLink().'" title="'.$item->getTitle().'">'
                            . $item->getLabel()
                            . '</a></li>';
                          }
                          ?>
                        </ul>
                        <ul class="usermenu">
                        <?php
                          $menu_items = (new \dokuwiki\Menu\UserMenu())->getItems();
                          foreach($menu_items as $item) {
                          echo '<li class="'.$item->getType().'">'
                            .'<a class="" href="'.$item->getLink().'" title="'.$item->getTitle().'">'
                            .'<i class="icon '.$item->getType().'" aria-hidden="true">'.'&nbsp;'.'</i>'
                            . '<span class="a11y">'.$item->getLabel().'</span>'
                            . '</a></li>';
                          }
                          ?>
                        </ul>
                      </nav>
                    </div>
                    <!-- wikipage stop -->
                    <?php tpl_includeFile('pagefooter.html') ?>


                </div>

                <?php tpl_flush() ?>
            </main><!-- /content -->
        </div><!-- /wrapper -->

        <?php include('tpl_footer.php') ?>
    </div></div><!-- /site -->

    <div class="no"><?php tpl_indexerWebBug() /* provide DokuWiki housekeeping, required in all templates */ ?></div>
    <div id="screen__mode" class="no"></div><?php /* helper to detect CSS media query in script.js */ ?>

    <script>

      // mobile navigation
      var navBurguer = document.getElementsByClassName('navbar-burger')[0];
      var sidebarNav = document.getElementsByClassName('sidebar')[0];
      var sidebarOverlay = document.getElementsByClassName('sidebar-overlay')[0];

      navBurguer.onclick = function() {
        navBurguer.classList.toggle('is-active');
        sidebarNav.classList.toggle('is-shown');
        sidebarOverlay.classList.toggle('is-shown');
      }

    </script>
</body>
</html>
