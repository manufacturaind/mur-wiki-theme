<?php
/**
 * Template header, included in the main and detail files
 */

// must be run from within DokuWiki
if (!defined('DOKU_INC')) die();
?>

<!-- ********** HEADER ********** -->
<navbar id="topnav">

    <?php tpl_includeFile('header.html') ?>


    <header id="dokuwiki__header">

        <div class="navbar-logo">
          <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false">
            <div>
              <span aria-hidden="true"></span>
              <span aria-hidden="true"></span>
              <span aria-hidden="true"></span>
            </div>
          </a>
          <div class="navbar-item" href="/">
            <?php
                // get logo either out of the template images folder or data/media folder
                $logoSize = array();
                $logo = tpl_getMediaFile(array(':wiki:logo.png', ':logo.png', 'images/logo.png'), false, $logoSize);

                // display logo and wiki title in a link to the home page
                tpl_link(
                    wl(),
                    '<img class="logo" src="'.$logo.'" '.$logoSize[3].' alt="" />' /* <span>'.$conf['title'].'</span>' */,
                    'accesskey="h" title="' . tpl_getLang('home') . ' [h]"'
                );
            ?>
            <?php if ($conf['tagline']): ?>
                <p><?php echo $conf['tagline']; ?></p>
            <?php endif ?>
          </div>
        </div><!-- /.navbar-logo -->

    </header>

    <div class="tools">
        <!-- USER TOOLS -->
        <?php if ($conf['useacl']): ?>
            <div id="dokuwiki__usertools">
                <ul>
                  <?php
    							if (!empty($_SERVER['REMOTE_USER'])) {
    								echo '<li class="item"> ';
    								tpl_userinfo();
    								echo '</li>';
    							}
    							?>
                  <?php
                    $menu_items = (new \dokuwiki\Menu\UserMenu())->getItems();
                    foreach($menu_items as $item) {
                    echo '<li class="'.$item->getType().'">'
                      .'<a class="nav-link" href="'.$item->getLink().'" title="'.$item->getTitle().'">'
                      .'<i class="icon '.$item->getType().'" aria-hidden="true">'.'&nbsp;'.'</i>'
                      . '<span class="a11y">'.$item->getLabel().'</span>'
                      . '</a></li>';
                    }
                    ?>
                </ul>
            </div>
        <?php endif ?>
    </div><!-- /.tools -->

</navbar><!-- /navbar -->
